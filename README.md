## USAGE

Start by running [docker-compose](https://docs.docker.com/compose/install/) inside the directory containing docker-compose.yml

1. `git clone --recurse-submodules https://bitbucket.org/ottov123/gcad-web-api.git`
2. `cd gcad-web-api`
3. `docker-compose up`

---

## Accessing running instance

Your instance of the api will accessible via localhost or public IP

[http://localhost/v1/projects]


---

## Adding data

Follow the steps below to add data

1. `curl`
